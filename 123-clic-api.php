<?php 
/*
* @author:<Frenley E>
* @Description: This page is about authenticating the request and then  giving access to respective api request pages
* @Date:2017-11-09
*/
require_once 'Mobile_Detect.php';
include('db.php');
include('logs.php');
include('common.php');
include('constants.php');

// check if request is coming from system or phone or tablet
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();

$requestType = '';
$fileLocation = '';
$userType = '';
$logParameters = array();
$responseArray = array();
header('Access-Control-Allow-Origin:*');
header('Content-type: application/json');
// get json data from request
$json = file_get_contents('php://input');
$data = json_decode($json,true);

$userType = '';
$userType = $data['userType'];
$command = $data['command'];
$responseArray = array();

// log class object creation
$logs = new logs();

// check request method type
if($_SERVER['REQUEST_METHOD'] === 'POST') {
	$requestType = 'POST';
} else if($_SERVER['REQUEST_METHOD'] === 'GET'){
	$requestType = 'GET';
};

// validation file
$common = new common();
$authTokenStatus = $common->checkAuthToken($data['authToken']);

// Get remote address 
$remoteAddress = $_SERVER["REMOTE_ADDR"];

if(empty($authTokenStatus)) {
	// entry for succeded authentication in log file
	$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'authentication',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'success',
			"Actual_Data_Received" => $json,
			"Data_Responded" => "NA"
	);
	$logs->create_log($logParameters,$userType);

	// check the command - which function request is calling
	
	
	if($userType == "customer") {

		$fileLocation = 'customer/';
		switch($command) {
			case 'login':
					// initial log for login page - customer
					$logParameters = array(
							"Request_Remote_Address" => $remoteAddress,
							"Requested_Page" => 'login',
							"Request_Method" => $requestType,
							"Request_Sent_From" => $deviceType,
							"Requested_Date_Time" => date('Y-m-d h:i:s'),
							"Request_Status" => 'Before Going Inside Login Page',
							"Actual_Data_Received" => $json,
							"Data_Responded" => "NA"
					);
					$logs->create_log($logParameters,$userType);
					$apiArray = array(
						"authToken" => "",
					    "command" => "",
					    "userType" => "",
					    "email" => "",
					    "password" => ""
					);

					$responseArray = $common->validateInput($requestType,$data,$apiArray);
					
					if (empty($responseArray)) {
    					include($fileLocation.'login.php');
					} else {
						echo json_encode($responseArray);
						die;
					}					
			break;

			case 'add_customer':
					include('register.php');
			break;

			case 'edit_customer':
			break;

			case 'category_list':
				// initial log for category_list page - customer
					$logParameters = array(
							"Request_Remote_Address" => $remoteAddress,
							"Requested_Page" => 'login',
							"Request_Method" => $requestType,
							"Request_Sent_From" => $deviceType,
							"Requested_Date_Time" => date('Y-m-d h:i:s'),
							"Request_Status" => 'Before Going Inside category_list Page',
							"Actual_Data_Received" => $json,
							"Data_Responded" => "NA"
					);
					$logs->create_log($logParameters,$userType);
					$apiArray = array(
						"authToken" => "",
					    "command" => "",
					    "userType" => ""
					);
					include('category_list.php');
			break;

			case 'sub_category_list':
					// initial log for sub_category_list page - customer
					$logParameters = array(
							"Request_Remote_Address" => $remoteAddress,
							"Requested_Page" => 'login',
							"Request_Method" => $requestType,
							"Request_Sent_From" => $deviceType,
							"Requested_Date_Time" => date('Y-m-d h:i:s'),
							"Request_Status" => 'Before Going Inside sub_category_list Page',
							"Actual_Data_Received" => $json,
							"Data_Responded" => "NA"
					);
					$logs->create_log($logParameters,$userType);
					$apiArray = array(
						"authToken" => "",
					    "command" => "",
					    "userType" => "",
					    "category_id" => ""
					);
					include('sub_category_list.php');
			break;

			case 'service_list':
			break;

			case 'my_reservations_list':
			break;

			case 'add_reservations':
			break;

			case 'add_booking':
			break;

			case 'booking_list':
			break;

		}
	} else if($userType == 'specialist') {

		$fileLocation = 'specialist/';
		switch($command) {
			case 'login':
					include($fileLocation.'login.php');
			break;

		}

	}

} else {
	// entry for failed authentication in log file
	$logParameters = array(
			"Request_Remote_Address" => $remoteAddress,
			"Requested_Page" => 'authentication',
			"Request_Method" => $requestType,
			"Request_Sent_From" => $deviceType,
			"Requested_Date_Time" => date('Y-m-d h:i:s'),
			"Request_Status" => 'failed',
			"Actual_Data_Received" => $json,
			"Data_Responded" => "NA"
	);
	$logs->create_log($logParameters,$userType);

	echo json_encode($authTokenStatus);
	die;
}
?>